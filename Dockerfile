FROM node:12.16.1-slim

RUN mkdir -p /app
WORKDIR /app
COPY ./src /app

RUN yarn

EXPOSE 3000

ENV NODE_ENV production
RUN node data/seed.js

ENTRYPOINT ["node"]
CMD ["server.js"]