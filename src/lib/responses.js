const STATUS_CODES = require('./status_codes');
exports.Unauthorized = (
  message = 'This account is not authorized to perform this action'
) => {
  return {
    status: STATUS_CODES.UNAUTHORIZED,
    message: message,
  };
};

exports.NotFound = (message = 'Requested resource was not found') => {
  return {
    status: STATUS_CODES.NOT_FOUND,
    message: message,
  };
};

exports.NotImplemented = (
  message = 'Requested resource was not implemented'
) => {
  return {
    status: STATUS_CODES.NOT_IMPLEMENTED,
    message: message,
  };
};

exports.BadRequest = (message = 'Bad Request - see errors', errors = []) => {
  return {
    status: STATUS_CODES.BAD_REQUEST,
    message: message,
    errors: errors,
  };
};

exports.Forbidden = (message = 'Forbidden - see errors', errors = []) => {
  return {
    status: STATUS_CODES.FORBIDDEN,
    message: message,
    errors: errors,
  };
};

exports.InternalError = (
  message = 'Something went wrong - please try again later'
) => {
  return {
    status: STATUS_CODES.INTERNAL_SERVER_ERROR,
    message: message,
  };
};

exports.Ok = (message = 'success') => {
  return {
    status: STATUS_CODES.OK,
    message: message,
  };
};

const Book = (book) => {
  return {
    id: book.id,
    title: book.title,
    isbn: book.isbn,
    isbn10: book.isbn10,
    author: book.author,
    htmlExcerpt: book.htmlExcerpt,
    checkedOutBy: book.Users,
  };
};

exports.BookResponse = (book) => {
  return {
    status: STATUS_CODES.OK,
    data: Book(book),
  };
};

exports.BooksResponse = (books) => {
  const bookRespArr = [];
  books.forEach((book) => {
    bookRespArr.push(Book(book));
  });
  const resp = {
    status: STATUS_CODES.OK,
    data: bookRespArr,
  };
  return resp;
};
