const router = require('express').Router();
const memberController = require('../controllers/member_controller');
const acl = require('../middlewares/access_control');
const { confirmExisting } = require('../middlewares/validation');
const { Books } = require('../models');

// member route 'ACL' middleware
router.use(acl.members);

// define member routes
router.post('/checkout/books/:book_id', [
  confirmExisting('book_id', Books),
  memberController.checkoutBook,
]);
router.post('/return/books/:book_id', [
  confirmExisting('book_id', Books),
  memberController.returnBook,
]);
router.post('/return-all/books', memberController.returnAllBooks);

module.exports = router;
