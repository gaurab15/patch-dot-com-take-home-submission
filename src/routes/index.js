const health = require('./health');
const documentation = require('./documentation');
const librarians = require('./librarians');
const members = require('./members');

const authorization = require('../middlewares/authorization');

const router = (app) => {
  // define public routes
  app.use('/', health);
  app.use('/docs', documentation);

  // global authoriztion middleware to load current user
  app.use(authorization);

  // define application routes
  app.use('/books', librarians);
  app.use('/members', members);
};

module.exports = router;
