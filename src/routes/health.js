const router = require('express').Router();

const health = (req, resp) => {
  resp.json({ status: 'OK' });
};

router.get('/', health);
router.get('/health', health);

module.exports = router;
