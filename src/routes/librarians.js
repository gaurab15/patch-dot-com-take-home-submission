const router = require('express').Router();
const librarianController = require('../controllers/librarian_controller');
const acl = require('../middlewares/access_control');
const {
  bodyRequired,
  validateProperties,
  requiredProperties,
  confirmExisting,
} = require('../middlewares/validation');
const { Books } = require('../models');

// librarian route 'ACL' middleware
router.use(acl.librarians);

//define librarian routes with validation and handlers
router.put('/', [
  bodyRequired,
  validateProperties(Books),
  requiredProperties(['isbn']),
  librarianController.addBook,
]);
router.delete('/:book_id', [
  confirmExisting('book_id', Books),
  librarianController.deleteBook,
]);
router.get('/overdue', librarianController.overdueBooks);

module.exports = router;
