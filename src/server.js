const env = process.env.NODE_ENV || 'development';
const port = process.env.PORT || 3000;

const express = require('express');
const { sequelize } = require('./models');
const router = require('./routes');
const bodyParser = require('body-parser');

const STATUS_CODES = require('./lib/status_codes');
const { NotFound } = require('./lib/responses');

// initialize express application
const app = express();

// use body-parser middleware to parse all requests as application/json
app.use(bodyParser.json());

// setup routes for the application
router(app);

// handle all other requests with a 404 NOT FOUND response
app.use((req, res, next) => {
  res.status(STATUS_CODES.NOT_FOUND).json(NotFound());
});

// handle all errors with their specific error codes
// handle non specific errors with a 500 Internal Server Error
app.use((err, _req, res, _next) => {
  if (!err.status) err.status = STATUS_CODES.INTERNAL_SERVER_ERROR;
  res.status(err.status).json(err);
});

// syncronize the server with the loaded models
(async () => {
  await sequelize.sync();
})();

// start the server on specified port
const server = app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});

module.exports = { app, server };
