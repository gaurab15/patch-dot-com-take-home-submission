const { Forbidden } = require('../lib/responses');

exports.members = async (req, _res, next) => {
  // reject if user account is librarian
  if (req.currentUser.isLibrarian) {
    next(Forbidden('Librarians cannot perform member actions'));
    return;
  }
  next();
};

exports.librarians = async (req, _res, next) => {
  // reject if user account is librarian
  if (!req.currentUser.isLibrarian) {
    next(Forbidden('Non Librarians cannot perform librarian actions'));
    return;
  }
  next();
};
