const { Unauthorized, NotFound } = require('../lib/responses');
const { Users } = require('../models');

module.exports = async (req, _res, next) => {
  // check if request headers are valid
  const userId = req.headers['user-id'];
  if (!userId) {
    next(Unauthorized());
    return;
  }

  // check if user exists in the database
  const user = await Users.findByPk(userId).catch(next);
  if (!user) {
    /**
     * NOTE:  For stricter security we should call next(Unauthorized())
     */
    next(NotFound('Requesting User not found'));
    return;
  }

  // set current user for subsequent middlewares and routes to use
  req.currentUser = user;
  next();
};
