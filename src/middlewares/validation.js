const { BadRequest, InternalError, NotFound } = require('../lib/responses');

exports.bodyRequired = async (req, _res, next) => {
  if (!req.body) {
    next(BadRequest('Bad Request', ['No JSON sent']));
  } else {
    next();
  }
};

exports.validateProperties = (model) => {
  const modelProperties = Object.keys(model.rawAttributes);
  return async (req, _res, next) => {
    for (const requestedProp of Object.keys(req.body)) {
      if (!modelProperties.includes(requestedProp)) {
        next(BadRequest('Unknown property', requestedProp));
      }
    }
    next();
  };
};

exports.requiredProperties = (propertiesArray) => {
  return async (req, _res, next) => {
    const errors = [];
    for (const property of propertiesArray) {
      if (!req.body[property]) {
        errors.push(property);
      }
    }
    if (errors.length) {
      next(BadRequest('Missing body fields in request', errors));
    } else {
      next();
    }
  };
};

exports.confirmExisting = (idPlaceHolder, model) => {
  return async (req, _res, next) => {
    const id = req.params[idPlaceHolder];
    const entity = await model.findByPk(id);
    if (!entity) {
      next(NotFound(`Could not find ${model.name} with id ${id}`));
    } else {
      // adding fetched instance of the model to the request
      if (!req.models) {
        req.models = {};
      }
      req.models[model.name] = req.models[model.name]
        ? req.models[model.name].push(entity)
        : [entity];
      next();
    }
  };
};
