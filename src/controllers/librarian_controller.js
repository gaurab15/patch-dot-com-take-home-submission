const { Books } = require('../models');
const {
  Ok,
  InternalError,
  BooksResponse,
  BadRequest,
} = require('../lib/responses');

const librarianController = {
  addBook: async (req, res, next) => {
    await Books.create({
      ...req.body,
    })
      .then((book) => {
        res.json(Ok('Book created'));
      })
      .catch((error) => {
        if (error.name == 'SequelizeValidationError') {
          const errItems = error.errors.reduce((arr, item) => {
            arr.push(item.message);
            return arr;
          }, []);
          next(BadRequest('Validation error', errItems));
        } else {
          next(InternalError(error.message));
        }
      });
  },
  deleteBook: async (req, res, next) => {
    const book_id = req.params.book_id;
    const removedRowCount = await Books.destroy({ where: { id: book_id } });
    if (removedRowCount) {
      res.json(Ok(`Removed ${removedRowCount} book with id ${book_id}`));
    } else {
      next(InternalError());
    }
  },
  overdueBooks: async (_req, res, _next) => {
    const overdueBooks = await Books.getOverdueBooks();
    res.json(BooksResponse(overdueBooks));
  },
};

module.exports = librarianController;
