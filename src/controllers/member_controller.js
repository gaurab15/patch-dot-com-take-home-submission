const {
  Ok,
  InternalError,
  NotImplemented,
  BadRequest,
} = require('../lib/responses');
const { Books, Users } = require('../models');

const memberController = {
  checkoutBook: async (req, res, next) => {
    // reload the user from the database to include the association
    const user = await req.currentUser.reload({ include: Books });

    // get the conditions that might stop a user from checking out books
    const checkoutStops = await user.getCheckoutStops();
    if (checkoutStops.length) {
      next(BadRequest('Cannot checkout book', checkoutStops));
      return;
    }

    // load the book (done for clarity, the model should already be loaded
    // by the route validation middleware)
    const book = await Books.findByPk(req.params.book_id);
    await user
      .checkoutBook(book)
      .then(() => {
        res.json(Ok('Sucessfully checked out book'));
      })
      .catch((error) => {
        console.log(error.name, error.message);
        if (error.name == 'AlreadyCheckedOutError') {
          next(BadRequest(error.message, error.errors));
        } else {
          next(InternalError());
        }
      });
  },
  returnBook: async (req, res, next) => {
    // reload the user from the database to include the association
    const user = await req.currentUser.reload({ include: Books });

    const book = await Books.findByPk(req.params.book_id);
    if (user.hasBook(book) === false) {
      next(
        BadRequest('Cannot return book', [
          'This user has not checked out this book previously',
        ])
      );
      return;
    }

    await user.returnBook(book).then((removedRowCount) => {
      if (removedRowCount) {
        res.json(Ok('Successfully returned book'));
      } else {
        next(InternalError());
      }
    });
  },
  returnAllBooks: async (req, res, next) => {
    const user = await req.currentUser.reload({ include: Books });
    const checkedOutBooks = await user.countBooks();

    if (!checkedOutBooks) {
      next(
        BadRequest(
          'Cannot return books',
          'User did not have any checked-out books'
        )
      );
      return;
    }

    await user.returnAllBooks().then(() => {
      res.json(Ok('Successfully returned all books checked out by this user'));
    });
  },
};

module.exports = memberController;
