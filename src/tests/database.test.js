const { sequelize, Books, Users, BookCheckouts } = require('../models');
const books_seed = require('../data/seed_data/books.json');

beforeAll(async (done) => {
  await sequelize.sync({ force: true });
  done();
});

const createTestUser = async () => {
  return await Users.create({
    name: 'user',
  });
};
const createTestBook = async () => {
  return await Books.create({
    title: 'Test Title',
    isbn: '1234567890123',
  });
};

describe('Check database functionality', () => {
  describe('Users table', () => {
    test('Add Users', async (done) => {
      const users = await Users.bulkCreate([
        { name: 'Larry', isLibrarian: true },
        { name: 'Alice' },
        { name: 'Bob' },
        { name: 'Charles' },
      ]);
      expect(users.length).toBe(4);
      done();
    });
  });

  describe('Books table', () => {
    test('Add to books', async (done) => {
      const books = await Books.bulkCreate(books_seed);
      expect(books.length).toEqual(books_seed.length);
      done();
    });

    test('Add duplicate ISBN books', async (done) => {
      const book1 = await createTestBook();
      const book2 = await createTestBook();

      expect(book1.name).toEqual(book2.name);
      expect(book1.isbn).toEqual(book2.isbn);
      expect(book1.id).not.toBe(book2.id);
      done();
    });

    test('Remove a book', async (done) => {
      const book = await createTestBook();
      const removedRows = await Books.destroy({ where: { id: book.id } });
      expect(removedRows).toEqual(1);
      done();
    });
  });

  describe('BookCheckouts table', () => {
    test('Checkout book', async (done) => {
      let alice = await Users.findOne({
        where: { name: 'Alice' },
      });
      const book = await Books.findOne({ where: { id: 50 } });
      const book2 = await Books.findOne({ where: { id: 55 } });
      const book3 = await Books.findOne({ where: { id: 60 } });
      await alice.addBook(book, { individualHooks: true });
      await alice.addBook(book2, { individualHooks: true });
      await alice.addBook(book3, { individualHooks: true });
      const checkedOutBook = await Books.findOne({
        where: { id: 50 },
        include: Users,
      });
      alice = await Users.findOne({
        where: { id: alice.id },
        include: Books,
      });
      expect(alice.Books[0].id).toBe(50);
      expect(checkedOutBook.Users[0].name).toBe('Alice');
      done();
    });

    test('Delete Checkout on Book deletion', async (done) => {
      let user = await createTestUser();
      const book = await createTestBook();
      await user.addBook(book, { individualHooks: true });
      await user.reload({ include: Books });

      await book.destroy().catch((e) => {
        expect(e.name).toBe('SequelizeUniqueConstraintError');
      });

      const bookCount = await user.countBooks();
      expect(bookCount).toBe(1);
      done();
    });

    test('Delete Checkout on User deletion', async (done) => {
      const user = await createTestUser();
      let book = await createTestBook();
      await user.addBook(book, { individualHooks: true });

      book = await Books.findOne({
        where: { id: book.id },
        include: Users,
      });

      await user.destroy().catch((e) => {
        expect(e.name).toBe('SequelizeUniqueConstraintError');
      });

      const userCount = await book.countUsers();
      expect(userCount).toBe(1);
      done();
    });
  });
});
