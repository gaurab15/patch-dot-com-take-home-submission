const { app, server } = require('../server');
const { Books, Users } = require('../models');
const request = require('supertest');
const moment = require('moment');

const createTestUser = async () => {
  return await Users.create({
    name: 'user',
  });
};

const createTestBook = async () => {
  return await Books.create({
    title: 'Test Title',
    isbn: '1234567890128',
  });
};

describe('Check application health endpoints', () => {
  test('GET /', async (done) => {
    const res = await request(app).get('/');
    expect(res.status).toBe(200);
    expect(res.body).toEqual({ status: 'OK' });
    done();
  });

  test('GET /health', async (done) => {
    const res = await request(app).get('/health');
    expect(res.status).toBe(200);
    expect(res.body).toEqual({ status: 'OK' });
    done();
  });
});

describe('Check librarian endpoints functionality', () => {
  test('PUT /books', async (done) => {
    const res = await request(app).put('/books').set('user-id', 1).send({
      isbn: '1234567890123',
      title: 'test120',
    });
    expect(res.status).toEqual(200);
    done();
  });

  test('DELETE book', async (done) => {
    const book = await Books.create({
      isbn: '123456789X',
      title: 'A book from before 2007',
    });
    const res = await request(app)
      .delete(`/books/${book.id}`)
      .set('user-id', 1);
    expect(res.body.status).toEqual(200);
    expect(res.body.message).toEqual(`Removed 1 book with id ${book.id}`);
    done();
  });

  test('GET overdue books', async (done) => {
    const user = await Users.create({
      name: 'overdueUser',
    });
    let book = await Books.create({
      title: 'Test Title',
      isbn: '1234567890122',
    });
    await user.addBook(book, {
      individualHooks: true,
      through: {
        checkoutDate: moment().subtract(28, 'days'),
        dueDate: moment().subtract(14, 'days'),
      },
    });
    const res = await request(app).get('/books/overdue').set('user-id', 1);
    expect(res.status).toEqual(200);
    expect(res.body.data.length).toBe(1);
    done();
  });
});

describe('Check member endpoints functionality', () => {
  test('GET /members/checkout/books/{bookId}', async (done) => {
    const newUser = await createTestUser();
    const res = await request(app)
      .post('/members/checkout/books/50')
      .set('user-id', newUser.id);
    const res2 = await request(app)
      .post('/members/checkout/books/51')
      .set('user-id', newUser.id);
    const res3 = await request(app)
      .post('/members/checkout/books/52')
      .set('user-id', newUser.id);
    const res4 = await request(app)
      .post('/members/checkout/books/53')
      .set('user-id', newUser.id);
    expect(res.status).toEqual(200);
    expect(res2.status).toEqual(200);
    expect(res3.status).toEqual(200);
    expect(res4.status).toEqual(400);
    done();
  });

  test('GET /members/checkout/books/{bookId} as librarian', async (done) => {
    const res = await request(app)
      .post('/members/checkout/books/asLibrarian')
      .set('user-id', 1);
    expect(res.status).toEqual(403);
    done();
  });

  test('POST /members/return/books/{bookId}', async (done) => {
    const user = await createTestUser();
    const book = await createTestBook();
    await user.addBook(book, { individualHooks: true });

    const res = await request(app)
      .post(`/members/return/books/${book.id}`)
      .set('user-id', user.id);
    expect(res.status).toEqual(200);
    done();
  });

  test('POST /members/books/return-all', async (done) => {
    const res = await request(app)
      .post('/members/return-all/books')
      .set('user-id', 2);
    expect(res.status).toEqual(200);
    done();
  });
});

afterAll(() => {
  server.close();
});
