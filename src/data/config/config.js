const path = require('path');

const db_config = {
  development: {
    database: 'library',
    dialect: 'sqlite',
    storage: path.join(__dirname, '..', 'db.dev.sqlite3'),
  },
  test: {
    database: 'library',
    dialect: 'sqlite',
    // storage: ':memory:',
    logging: false,
    storage: path.join(__dirname, '..', 'db.test.sqlite3'),
  },
  production: {
    database: 'library',
    dialect: 'sqlite',
    storage: path.join(__dirname, '..', 'db.sqlite3'),
  },
};

module.exports = db_config;
