const moment = require('moment');
const { Op } = require('sequelize');
const { Users, Books, sequelize } = require('../models');
const seed_users = require('./seed_data/users.json');
const seed_books = require('./seed_data/books.json');

(async () => {
  // drop any existing table
  await sequelize.sync({ force: true });

  // add seed data
  await Users.bulkCreate(seed_users);
  await Books.bulkCreate(seed_books);

  // fetch added users
  const alice = await Users.findOne({
    where: {
      id: 2,
    },
  });

  const bob = await Users.findOne({
    where: {
      id: 3,
    },
  });

  const charles = await Users.findOne({
    where: {
      id: 4,
    },
  });

  // select 5 random books
  const randomNumbers = [];
  for (let i = 0; i < 5; i++) {
    randomNumbers.push(Math.floor(Math.random() * seed_books.length));
  }

  const randomBooks = await Books.findAll({
    where: {
      id: {
        [Op.or]: randomNumbers,
      },
    },
  });

  // add alice book checkout
  await alice.addBook(randomBooks[0], { individualHooks: true });

  // add bob overdue book
  await bob.addBook(randomBooks[1], {
    individualHooks: true,
    through: {
      checkoutDate: moment().subtract(28, 'days'),
      dueDate: moment().subtract(14, 'days'),
    },
  });

  // add charles multiple books
  await charles.addBook(randomBooks[2], { individualHooks: true });
  await charles.addBook(randomBooks[3], { individualHooks: true });
  await charles.addBook(randomBooks[4], { individualHooks: true });
})().then(() => {
  console.log('Finished seeding data');
});
