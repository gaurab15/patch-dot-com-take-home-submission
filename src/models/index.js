const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const sequelize = require('../data/connection');
const Books = require('../models/books');
const Users = require('../models/users');
const BookCheckouts = require('./book_checkouts');

const models = {
  /**
   * @type {Books}
   */
  Books: Books.init(sequelize),
  /**
   * @type {Users}
   */
  Users: Users.init(sequelize),
  /**
   * @type {BookCheckouts}
   */
  BookCheckouts: BookCheckouts.init(sequelize),
};

// iterate through the models and run their respective associations and hooks
Object.keys(models).forEach((modelName) => {
  if (models[modelName].associate) {
    models[modelName].associate(models);
  }
  if (models[modelName].hooks) {
    models[modelName].hooks();
  }
});

models.sequelize = sequelize;

module.exports = models;
