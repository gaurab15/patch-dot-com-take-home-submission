const { DataTypes, Model, Op } = require('sequelize');
const Users = require('./users');
const moment = require('moment');

class Books extends Model {
  /**
   * Use this method to setup the model
   *
   * @param {Sequelize} sequelize The configured sequelized ORM object
   */
  static init(sequelize) {
    return super.init(
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER,
          validate: {
            isInt: true,
          },
        },
        title: DataTypes.STRING(255),
        isbn: {
          allowNull: false,
          type: DataTypes.STRING(20),
          validate: {
            notEmpty: true,
            isValidISBN(value) {
              const dashStrippedAndTrimmed = String(value)
                .replace(/-/g, '')
                .trim();
              console.log(dashStrippedAndTrimmed);
              const length = dashStrippedAndTrimmed.length;
              if (length != 10 && length != 13) {
                throw new Error('invalid ISBN format');
              }
            },
          },
        },
      },
      { sequelize, modelName: 'Books', tableName: 'books' }
    );
  }

  /**
   * Use this method to add associations between models in the database
   * This will be called after all models have been initiated
   *
   * @param {[Model]} models The models initiated in the ORM
   */
  static associate(models) {
    Books.belongsToMany(models.Users, {
      through: models.BookCheckouts,
      foreignKey: 'bookId',
      onDelete: 'SET NULL',
    });
  }

  static async getOverdueBooks() {
    const now = moment().format('YYYY-MM-DD');
    return await this.findAll({
      include: [
        {
          model: Users,
        },
      ],
      where: {
        $dueDate$: {
          [Op.lt]: now,
        },
      },
    });
  }
}

module.exports = Books;
