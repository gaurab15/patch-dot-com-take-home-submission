const { DataTypes, Model, Op } = require('sequelize');
const moment = require('moment');

class Users extends Model {
  /**
   * Use this method to setup the model
   *
   * @param {Sequelize} sequelize The configured sequelized ORM object
   */
  static init(sequelize) {
    return super.init(
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER,
          validate: {
            isInt: true,
          },
        },
        name: {
          allowNull: false,
          type: DataTypes.STRING(255),
        },
        isLibrarian: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
      },
      { sequelize, modelName: 'Users', tableName: 'users' }
    );
  }

  /**
   * Use this method to add associations between models in the database
   * This will be called after all models have been initiated
   *
   * @param {[Model]} models The models initiated in the ORM
   */
  static associate(models) {
    Users.belongsToMany(models.Books, {
      through: models.BookCheckouts,
      foreignKey: 'userId',
      onDelete: 'SET NULL',
    });
  }

  /**
   * Returns the condtions or "stops" that disallow this account from checking
   * out a book
   */
  async getCheckoutStops() {
    const stops = [];
    const checkedOutBooks = await this.countBooks();
    if (checkedOutBooks >= 3) {
      stops.push('User already has 3 or more books checked out');
    }
    // const overdueBooks = await this.Books
    const overdueBooks = this.Books.filter((book) => {
      return (
        moment(book.BookCheckouts.dueDate, 'YYYY-MM-DD') <
        moment().format('YYY-MM-DD')
      );
    });
    if (overdueBooks.length) {
      stops.push(`User has book(s) overdue`);
    }

    return stops;
  }

  /**
   * Returns the condtions or "stops" that disallow this account from checking
   * out a book
   */
  async checkoutBook(book) {
    if (this.hasBook(book)) {
      const error = new Error('This book is already checked out');
      error.name = 'AlreadyCheckedOutError';
      error.errors = [
        `Book with book id ${book.id} is already checked out by this user`,
      ];
      throw error;
    }

    const checkoutDate = moment();
    await this.addBook(book, {
      individualHooks: true,
      through: {
        checkoutDate: checkoutDate,
        dueDate: checkoutDate.add(14, 'days'),
      },
    });
  }

  hasBook(book) {
    for (const b of this.Books) {
      if (b.id == book.id) {
        return true;
      }
    }
    return false;
  }

  async returnBook(book) {
    return await this.removeBook(book);
  }

  async returnAllBooks() {
    return await this.setBooks([]);
  }
}

module.exports = Users;
