const moment = require('moment');
const sequelize = require('../data/connection');
const { DataTypes, Model, Op } = require('sequelize');

class BookCheckouts extends Model {
  static init(sequelize) {
    return super.init(
      {
        checkoutDate: {
          type: DataTypes.DATEONLY,
          defaultValue: DataTypes.NOW,
        },
        dueDate: {
          type: DataTypes.DATEONLY,
        },
      },
      { sequelize, modelName: 'BookCheckouts', tableName: 'book_checkouts' }
    );
  }
  static hooks() {
    this.addHook('beforeSave', (checkout, options) => {
      if (!checkout.dueDate) {
        checkout.dueDate = moment(checkout.checkoutDate, 'YYYY-MM-DD').add(
          2,
          'weeks'
        );
      }
    });
  }
}

module.exports = BookCheckouts;
