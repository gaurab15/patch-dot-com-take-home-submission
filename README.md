# Patch.com Take Home Challenge

## Contents

-   [Technologies Used](#techonologies-used)
-   [Approach](#approach)
    -   [Assumptions and Definitions](#assumptions-and-definitions)
    -   [UML Models](#uml-models)
        -   [Usecases](#usecases)
        -   [Entity Relation](#entity-relation)
    -   [Application Setup](#application-setup)
        -   [Endpoints](#endpoints)
        -   [Requests](#requests)
    -   [Project Layout](#project-layout)
-   [Installation](#installation)
    -   [with docker-compose](#docker-compose) (recommended)
    -   [with docker](#docker)
    -   [with npm/yarn](#npm/yarn)
-   [Documentation](#documentation)
    -   [Data](#data)
    -   [Test Requests](#test-requests)
        -   [Add a book](#add-a-book)
        -   [Remove a book](#remove-a-book)
        -   [Generate list of overdue books](#generate-list-of-overdue-books)
        -   [Checkout a book](#checkout-a-book)
        -   [Return a book](#return-a-book)
        -   [Return all books](#return-all-books)

# Technologies Used

![Node.js-v12.16.1](https://img.shields.io/badge/Node.js-v12.16.1-green.svg?logo=node.js&logoColor=yellow 'Node.js-v12.16.1')
![express-v4.17.1](https://img.shields.io/badge/express-v4.17.1-green.svg 'express v4.17.1')
![sequelize-v5.21.7](https://img.shields.io/badge/sequelize-v5.21.7-green.svg 'sequelize-v5.21.7')
![sqlite3-v4.2.0](https://img.shields.io/badge/sqlite3-v4.2.0-green.svg 'sqlite3-v4.2.0')

![swagger-v4.1.4](https://img.shields.io/badge/swagger-v4.1.4-green.svg?logo=swagger&logoColor=green 'swagger-v4.1.4')
![jest-v25.5.3](https://img.shields.io/badge/jest-v25.5.3-green.svg?logo=jest&logoColor=red 'jest-v25.5.3')

![docker-v19.03.8](https://img.shields.io/badge/docker-v19.03.8-green.svg?logo=docker&logoColor=blue 'docker-v19.03.8')
![docker-compose-v1.25.4](https://img.shields.io/badge/docker--compose-v1.25.4-green.svg 'docker-compose-v1.25.4')

# Approach

The idea is to create an **API only** backend which satisfies the conditions set in the [Challenge](./Challenge.md).

The backend server is created with the Node.js Express framework with Sequelize used for the ORM mappings to an sqlite3 database

## Assumptions and Definitions

-   Definitions:

    -   Librarians and Regular users (as per the [Challenge](./Challenge.md)) are _referred to_ as `Librarians` and `Members`, respectively, in the application nomenclature schemes
    -   Both Librarians and Members are _defined_ as `Users` in the model scope with the attribute `isLibrarian` separating the roles

-   **_ALL_** requests are:

    -   `Content-Type: application/json`
    -   assumed to be made by a previously logged in user and therefore can include `user-id: {userId}` in the request header to identify themselves. Requests without this field will fail with a `401 Unauthorized` status code

-   **_ALL_** responses are:

    -   `Content-Type: application/json`

-   Librarians cannot perform member actions:

    -   an account with `isLibrarian: true` cannot be used to checkout or return books
    -   NOTE: A librarian **_could_** checkout a book _for_ a member through a different endpoint (**NOT IMPLEMENTED IN THIS VERSION**)

-   A single Book can be checked out by multiple users

    -   a single book is defined as a book with a unique internal `id` in the database
    -   Rationality behind this is for a usecase of checking out digital copies of books

-   Cannot delete a User or a Book if either has a checkout record

    -   NOTE: a database TRIGGER (or ORM hook) **_could_** be implemented which would preserve the checkout details to a separate table/log for posterity before deleting the checkout record and **subsequently** the User and/or Book record (**NOT IMPLEMENTED IN THIS VERSION**)

-   Returning a checked out book deletes the record of that checkout from the database

    -   the database TRIGGER (or ORM hook) solution mentioned above applies here as well

-   No referential integrity assumed between duplicate ISBN entries (i.e. an ISBN can refer to multiple titles and updates on one will not cascade to the others)

    -   errors in ISBN issuance<sup>[wiki](https://en.wikipedia.org/wiki/International_Standard_Book_Number#Errors_in_usage)</sup> means one ISBN could potentially refer to works with different titles and authors

## UML Models

### Usecases

![usecase.png](./design_docs/usecases/usecase.png)

### Entity Relation

![usecase.png](./design_docs/entity_relations/entities.png)

## Application Setup

### Endpoints

See [documentation section](#documentation) for details.

#### Health

> GET /<br>
> GET /health<br>
> Returns the health of the application

#### Librarians

Follows RESTful pattern

> PUT /books<br>
> Add a book to the library

> DELETE /books/{bookId}<br>
> Delete a book matching `bookId`

> GET /books/overdue<br>
> Returns all the overdue books

#### Members

Follows controller-action-identifier pattern

> POST /members/checkout/books/{bookId}<br>
> Checkout a book matching `bookId`

> POST /members/return/books/{bookId}<br>
> Return a book matching `bookId`

> POST /members/return-all/books<br>
> Returns all the books

### Requests

## Project Layout

Project follows basic MVC structure with the views implemented as JSON responses in the controllers.

```bash
├── design_docs                         // Plant UML files
│   ├── entity_relations
│   └── usecases
├── src                                 // nodejs application root
│   ├── controllers
│   │   ├── librarian_controller.js     // librarians endpoints implemented here
│   │   └── member_controller.js        // members endpoints implemented here
│   ├── data                            // database and configurations
│   │   ├── config
│   │   ├── seed_data
│   │   └── connection.js               // ORM connection
│   ├── lib
│   │   ├── responses.js                // API Response Objects
│   │   └── status_codes.js
│   ├── middlewares
│   │   ├── access_control.js           // acl rules
│   │   ├── authorization.js            // global authorization
│   │   └── validation.js               // route validations
│   ├── models
│   │   ├── book_checkouts.js
│   │   ├── books.js
│   │   ├── index.js                    // require this file to use ORM
│   │   └── users.js
│   ├── routes
│   │   ├── health.js                   // API health endpoint implemented here
│   │   └── index.js                    // main router defined here
│   │   └── librarians.js               // librarian endpoints defined here
│   │   └── members.js                  // member endpoints defined here
│   ├── tests
│   ├── package.json
│   ├── swagger.json                     // API documentation config
│   └── server.js                        // Node entrypoint
├── Challenge.md
├── docker-compose.yml
├── Dockerfile
└── README.md
```

# Installation

-   [with docker-compose](#docker-compose) (recommended)
-   [with docker](#docker)
-   [with npm/yarn](#npm/yarn)

## docker-compose

```console
$ docker-compose up
```

The API will be available at [http://localhost:3000/](http://localhost:3000/)

## docker

Replace `[IMAGE_NAME]` and `[LOCAL_PORT]` below with values that do not confilct with your local setup

```console
$ docker build -t [IMAGE_NAME] .
$ docker run -p [LOCAL_PORT]:3000 -t [IMAGE_NAME]
```

The API will be available at http<nolink>://localhost:[LOCAL_PORT]/

## npm/yarn

Change into the `src/` directory

```console
$ cd src
```

then run one of the following:

npm:

```console
$ PORT=3000 npm start
```

_OR_ yarn:

```console
$ PORT=3000 yarn start
```

The API will be available at [http://localhost:3000/](http://localhost:3000/)

# Documentation

After running the project with the preferred method above - the API reference, documentation and live Playground should be available on http://localhost:3000/docs via the Swagger UI

## Data

The database is pre-seeded with the Users mentioned in the [Challenge](./Challenge.md) along with 100 books sourced from [RandomHouse](https://reststop.randomhouse.com/resources/titles?start=0&max=100&search=technology&expandLevel=1)

Upon first run the checkouts look like so:

-   userId 1 is a librarian so it does not have any checkouts
-   userId 2 has one randomly selected book checked out which is due in the future
-   userId 3 has one randomly selected book checked out which is overdue
-   userId 4 has three randomly selected books checked out which are all due in the future

## Test Requests

### Add a book

```console
$ curl -X PUT "http://localhost:3000/books" -H "accept: application/json" -H "user-id: 1" -H "Content-Type: application/json" -d "{ \"title\": \"Test Title from API UI\", \"isbn\": \"1-234567890123-\"}"
```

### Remove a book

```console
$ curl -X DELETE "http://localhost:3000/books/101" -H "accept: application/json" -H "user-id: 1"
```

### Generate list of overdue books

```console
$ curl -X GET "http://localhost:3000/books/overdue" -H "accept: application/json" -H "user-id: 1"
```

### Checkout a book

```console
$ curl -X POST "http://localhost:3000/members/checkout/books/1" -H "accept: application/json" -H "user-id: 2"
```

### Return a book

```console
$ curl -X POST "http://localhost:3000/members/return/books/1" -H "accept: application/json" -H "user-id: 2"
```

### Return all books

```console
$ curl -X POST "http://localhost:3000/members/return-all/books" -H "accept: application/json" -H "user-id: 4"
```
